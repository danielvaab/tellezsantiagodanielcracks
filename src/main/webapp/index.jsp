<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
		 pageEncoding="UTF-8"%>
<html>
<html lang="en">
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Navegacion con tabs</title>
	<link rel="stylesheet" href="estilos.css">
	<link rel="stylesheet" href="font-awesome.css">

	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="main.js"></script>
	<script src="BotonAceptarStakeh.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<script language="JavaScript">

		$( function() {
    $("#selectaccionstake").change( function() {
        if ($(this).val() === "AÑADIR") {
			$("#idstakeh").prop("disabled", false);
			$("#nombrestakeh").prop("disabled", false);
			$("#direccionstakeh").prop("disabled", false);
			$("#telefonostakeh").prop("disabled", false);
			$("#generostakeh").prop("disabled", false);
			$("#passstakeh").prop("disabled", false);
			document.getElementById('eliminarstakh').style.display = '';
			document.getElementById('aceptarstakh').style.display = 'none';
		}
		if ($(this).val() === "VER") {
            $("#idstakeh").prop("disabled", false);
			$("#nombrestakeh").prop("disabled", true);
			$("#direccionstakeh").prop("disabled", true);
			$("#telefonostakeh").prop("disabled", true);
			$("#generostakeh").prop("disabled", true);
			document.getElementById('eliminarstakh').style.display = 'none';
			document.getElementById('aceptarstakh').style.display = 'none';
		}
		if ($(this).val() === "CAMBIAR") {
            $("#idstakeh").prop("disabled", false);
			$("#nombrestakeh").prop("disabled", false);
			$("#direccionstakeh").prop("disabled", false);
			$("#telefonostakeh").prop("disabled", false);
			$("#generostakeh").prop("disabled", false);
			$("#passstakeh").prop("disabled", false);
			document.getElementById('eliminarstakh').style.display = 'none';
			document.getElementById('aceptarstakh').style.display = '';
		}
		if ($(this).val() === "ELIMINAR") {
            $("#idstakeh").prop("disabled", false);
			$("#nombrestakeh").prop("disabled", true);
			$("#direccionstakeh").prop("disabled", true);
			$("#telefonostakeh").prop("disabled", true);
			$("#generostakeh").prop("disabled", true);
			document.getElementById('eliminarstakh').style.display = '';
			document.getElementById('aceptarstakh').style.display = 'none';
        }
	});
	$("#selectacccont").change( function() {
        if ($(this).val() === "AÑADIR1") {
			document.getElementById('selectcontra').style.display = '';
			document.getElementById('selectcont').style.display = 'block';
			document.getElementById('selectcont2').style.display = 'block';
			document.getElementById('buscacont').style.display = 'block';
			alert("my debug text");
		}
		if ($(this).val() === "CREARCONT") {
            document.getElementById('selectcontra').style.display = '';
			document.getElementById('selectcont').style.display = 'block';
			document.getElementById('selectcont2').style.display = 'block';
		}
		if ($(this).val() === "ELIMINARCONT") {
            document.getElementById('selectcontra').style.display = '';
			document.getElementById('selectcont').style.display = 'block';
			document.getElementById('selectcont2').style.display = 'block';
		}
    });
});
	</script>
</head>
<body>
	<div class="wrap">
		<ul class="tabs">
			<li><a href="#tab1"><span class="fa fa-group"></span><span class="tab-text">GESTIÓN DE STAKEHOLDERS</span></a></li>
			<li><a href="#tab2"><span class="fa fa-book"></span><span class="tab-text">GESTIÓN DE CONTRATOS</span></a></li>
			<li><a href="#tab3"><span class="fa fa-stethoscope"></span><span class="tab-text">GESTIÓN DE CITAS MÉDICAS</span></a></li>
			<li><a href="#tab4"><span class="fa fa-male"></span><span class="tab-text">INFORMACIÓN PERSONAL</span></a></li>
		</ul>

		<div class="secciones">

			<!--STAKEHOLDERS-->
			<article id="tab1">
				<h1>GESTIÓN DE STAKEHOLDERS</h1>
				<br>
				<!--<form class="divstakeh" id="divstakeh" action="/StakehServlet" method="post"> -->
				<h2>SELECCIONE STAKEHOLDER</h2>
				<br>
				<div class="selectstakeholders" >
					<select class="selectstake" name="selectstake" id="selectstake">
						<option value="NADA">--SELECT--</option>
						<option value="PACIENTE">COTIZANTE</option>
						<option value="MEDICO">MÉDICO</option>
						<option value="ADMINISTRADOR">ADMINISTRADOR</option>
					</select>
				</div>
				<h2>SELECCIONE ACCIÓN</h2>
				<div class="selectaccionstakeholders">
					<select id="selectaccionstake" name="selectaccionstake">
						<option value="">--SELECT--</option>
						<option value="AÑADIR">AÑADIR</option>
						<option value="VER">VER</option>
						<option value="CAMBIAR">CAMBIAR</option>
						<option value="ELIMINAR">ELIMINAR</option>
					</select>
					<br>
					<br>
					<a href="#" class="cancelarstakeh">CANCELAR</a>
					<a href="#" class="aceptarstakeh">ACEPTAR</a>
					<br>
				</div>
				<br>

					ID: <input id= "idstakeh" class="idstakeh" name="idstakeh" placeholder="Id" disabled /> <button onclick="BotonAceptarStakeh1()" class="buscarstakh">VER</button> <button onclick="ingresaFuncion()" id="eliminarstakh" class="eliminarstakh" >CREAR</button> <a href="#" id="aceptarstakh" class="aceptarstakh" style="display: none;" action="BotonAceptarStakeh();">BUSCAR</a> <br>
					NOMBRE: <input id="nombrestakeh" name="nombrestakeh" class="nombrestakeh" placeholder="Nombre" disabled/> <br>
					DIRECCIÓN: <input id="direccionstakeh" class="direccionstakeh" name="direccionstakeh" placeholder="Dirección" disabled/><br>
					TELÉFONO: <input id="telefonostakeh" class="telefonostakeh" name="telefonostakeh" placeholder="Teléfono" disabled/><br>
					GÉNERO: <input id="generostakeh" class="generostakeh" name="generostakeh" placeholder="Género" disabled/><br>
					CONTRASEÑA: <input id="passstakeh" class="pastakeh" name="passstakeh" placeholder="Contraseña" disabled/><br>
				  <!--</form>-->
			</article>
			<!--CONTRATOS-->
			<article id="tab2">
				<h1>GESTIÓN DE CONTRATOS</h1>
				<br>
				<div id="selectaccioncontrato" class="selectaccioncontrato">
						<select class="selectacccont" name="selectacccont">
							<option value="">--SELECT--</option>
							<option value="AÑADIR1">AÑADIR</option>
							<option value="CREARCONT">CREAR CONTRATO</option>
							<option value="ELIMINARCONT">ELIMINAR CONTRATO</option>
						</select>
					</div>
				<h2>NUEVO BENEFICIARIO</h2>
					ID: <input class="idcont" name="idcont" id="idcont" placeholder="Id"/>   <br>
					NOMBRE: <input class="nombrecont" name="nombrecont" id="nombrecont" placeholder="Nombre"/> <br>
					FECHA DE NACIMIENTO: <input class="fechanaccont" name="fechanaccont" id="fechanaccont" placeholder="Fecha de nacimiento"/><br>
					TELÉFONO: <input class="telefonocont" name="telefonocont" id="telefonocont" placeholder="Teléfono"/><br>
					GÉNERO: <input class="generocont" name="generocont" id="generocont" placeholder="Género"/><br>
					CONTRASEÑA: <input class="contracont" name="contracont" id="contracont" placeholder="Contraseña"/><br>
				<h2>EPS</h2>
				<br>

				<div id="selectcont1" class="selectcont">
					<!--<select id="selectcontra" class="selectcontra" name="selectcont"></select>
					<a href="#" class="buscacont" >AÑADIR BENEFICIARIO</a>-->

					<!--FOR PARA AÑADIR TODAS LAS EPS-->
					<input class="epscont" name="epscont" id="epscont" placeholder="EPS"/><br>



				</div>
				<br>
				<h2>CONTRATO</h2>
				<br>
						CÓDIGO DE CONTRATO: <input class="codcont" id="codcont" name="codcont" placeholder="Código de contrato"/>   <br>
						CÓDIGO DE IPSOPCIONAL: <input class="codips" id="codips" name="codips" placeholder="Código de ips"/>   <br>

					<button onclick="BotonCrearContrato()" class="buscarstakh">CREAR CONTRATO</button>

			</article>
			<!--CITAS MÉDICAS-->
			<article id="tab3">
				<h1>GESTIÓN DE CITAS MÉDICAS</h1>
				<br>
				<h2>SELECCIONE EL MÉDICO</h2>
				<br>
				<form class="citasmeds" action="/CitasServlet" method="post">
				<div class="selectmedico">
						<select class="selectmed" name="selectmed">

						<!--FOR PARA AÑADIR TODAS LOS MÉDICOS-->


						</select>
				</div>
				<br>
				<h2>SELECCIONE LA HORA</h2>
				<br>
				<div class="selecthora">
						<select class="selecth" name="selecth">

						<!--FOR PARA AÑADIR TODAS LAS HORAS-->


						</select>
						<a href="#" class="agendacita">AGENDAR CITA MÉDICA</a>
				</div>
				</form>
			</article>
			<!--INFORMACIÓN PERSONAL-->
			<article id="tab4">
				<h1>INFORMACIÓN PERSONAL</h1>
				<br>
				<form class="divinfo">
						ID: <input class="idinfo" placeholder="Id" /><br>
						NOMBRE: <input class="nombreinfo" placeholder="Nombre" /> <br>
						FECHA DE NACIMIENTO: <input class="fechanacinfo" placeholder="Fecha de nacimiento" /><br>
						TELÉFONO: <input class="telefonoinfo" placeholder="Teléfono" /><br>
						GÉNERO: <input class="generoinfo" placeholder="Género" /><br>
					  </form>

			</article>
		</div>
	</div>
</body>
</html>