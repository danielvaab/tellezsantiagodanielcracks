package launch;

public class Franja {

    private String status;
    private Cita citica;

    public Franja(String status) {
        this.status = status;
        this.citica = null;
    }

    //GETTERS
    public String getStatus() {
        return status;
    }

    public Cita getCitica() {
        return citica;
    }

    //SETTERS
    public void setStatus(String status) {
        this.status = status;
    }

    public void setCitica(Cita citica) {
        this.citica = citica;
    }

    //TOSTRING
    @Override
    public String toString() {
        return "Franja{" + "status='" + status + ", citica=" + citica + '}';
    }
}
