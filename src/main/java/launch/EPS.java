package launch;

import java.util.ArrayList;

public class EPS implements Component {

    private String codEPS;
    private ArrayList<Component> componentes;

    public EPS(String codEPS) {
        this.codEPS = codEPS;
        this.componentes = new ArrayList();
    }
    public EPS() {
        this.codEPS = codEPS;
        this.componentes = new ArrayList();
    }

    //GETTERS
    public String getCodEPS() {
        return codEPS;
    }

    public ArrayList<Component> getComponentes() {
        return componentes;
    }

    //SETTERS
    public void setCodEPS(String codEPS) {
        this.codEPS = codEPS;
    }

    public void setComponentes(ArrayList<Component> componentes) {
        this.componentes = componentes;
    }



    public void adicionarcomp(Component contrato) {
        this.componentes.add(contrato);

    }



    //MÉTODOS  DE LA INTERFAZ
    @Override
    public String mostrarcon() {
        return null;
    }

    @Override
    public String mostrarper(String codCont) {
        return null;
    }
}
