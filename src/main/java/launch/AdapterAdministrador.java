package launch;

public class AdapterAdministrador extends Stakeholder {

    private Administrador admin;

    public AdapterAdministrador(String pass, String login, String documento, String numero, String genero, String direccion) {
        super();
        this.admin = new Administrador(pass,login,documento,numero,genero,direccion);
    }

    public AdapterAdministrador(){

    }



    @Override
    public String getPass() {
        return admin.getcontra();
    }

    @Override
    public void setPass(String pass) {
        admin.setcontra(pass);
    }

    @Override
    public String getLogin() {
        return admin.getinicio();
    }

    @Override
    public void setLogin(String login) {
        admin.setinicio(login);
    }

    @Override
    public String getdocumento() {
        return admin.getdocumento1();
    }

    @Override
    public void setdocumento(String document) {
        this.admin.setdocument1(document);
    }

    @Override
    public boolean DoLogin(String pass, String login) {
        return admin.Doinicio(pass,login);
    }

    @Override
    public String gettipousuario() {
        return admin.gettiuser();
    }

    @Override
    public String getNumero() {
        return admin.getnumero();
    }

    @Override
    public void setNumero(String numero) {
        this.admin.setnumero(numero);
    }

    @Override
    public String getGenero() {
        return admin.getgenero();
    }

    @Override
    public void setGenero(String genero) {
        this.admin.setGenero(genero);
    }

    @Override
    public String getDireccion() {
        return admin.getdireccion();
    }

    @Override
    public void setDireccion(String direccion) {
        this.admin.setdireccion(direccion);
    }
}
