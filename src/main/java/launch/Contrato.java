package launch;

import java.util.ArrayList;

public class Contrato implements Component {

    Cotizante cot = new Cotizante();
    private String codCont;

    private ArrayList<Beneficiario> beneficiarios;

    public Contrato(String codCont){
        this.codCont = codCont;
        this.beneficiarios = new ArrayList();
    }

    public Contrato(){
        this.codCont = "";
        this.beneficiarios = new ArrayList();
    }

    // GETTERS
    public Cotizante getCot() {
        return cot;
    }

    public String getCodCont() {
        return codCont;
    }

    public ArrayList<Beneficiario> getBeneficiarios() {
        return beneficiarios;
    }

    //SETTERS
    public void setCot(Cotizante cot) {
        this.cot = cot;
    }

    public void setCodCont(String codCont) {
        this.codCont = codCont;
    }

    public void setBeneficiarios(ArrayList<Beneficiario> beneficiarios) {
        this.beneficiarios = beneficiarios;
    }




    public void adicionarben(String codCont, Beneficiario beneficiario) {

        this.beneficiarios.add(beneficiario);
    }

    //MÉTODOS DEL INTERFAZ

    @Override
    public String mostrarcon() {
        return null;
    }

    @Override
    public String mostrarper(String codCont) {
        return null;
    }
}
