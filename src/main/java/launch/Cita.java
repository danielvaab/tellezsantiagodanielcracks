package launch;

public class Cita {

    private Medico medico;
    private Paciente paciente;

    public Cita(Medico med, Paciente pac) {
        this.medico = med;
        this.paciente = pac;
    }

    //GETTERS
    public Medico getMedico() {
        return medico;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    //SETTERS
    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    @Override
    public String toString() {
        return "Cita{" + "medico=" + medico + ", paciente=" + paciente + '}';
    }
}
