package launch;

public class Medico extends Stakeholder{
    private String documento;
    private String pass;
    private String login;
    private String numero="";
    private String direccion="";
    private String genero="";
    private Calendario calendario;

    public Medico() {
        super();
        this.documento = "";
        this.pass = "";
        this.login = "";
        this.numero = "";
        this.direccion = "";
        this.genero = "";
        this.calendario = new Calendario();
    }

    public Medico(String pass, String documento, String login,  String numero, String genero, String direccion) {
        super();
        this.documento = documento;
        this.pass = pass;
        this.login = login;
        this.numero = numero;
        this.direccion = direccion;
        this.genero = genero;
        this.calendario = new Calendario();
    }

    //GETTERS
    public Calendario getCalendario() {
        return calendario;
    }
    @Override
    public String getPass() {
        return pass;
    }

    @Override
    public String getLogin() {
        return login;
    }


    @Override
    public String gettipousuario() {
        return "paciente";
    }

    @Override
    public String getGenero() {
        return genero;
    }

    @Override
    public String getDireccion() {
        return direccion;
    }

    @Override
    public String getNumero() {
        return numero;
    }

    @Override
    public String getdocumento() {
        return documento;
    }

    //SETTERS
    @Override
    public void setPass(String pass) {
        this.pass = pass;
    }

    @Override
    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public void setdocumento(String documento) {
        this.documento = documento;
    }

    @Override
    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public boolean DoLogin(String pass, String login) {
        return pass.equals(this.pass) && login.equals(this.login);
    }

    public void setCalendario(Calendario calendario) {
        this.calendario = calendario;
    }
    //AGREGAR CITA

    public void agregarDia(String fecha, float horaIni, float horaFin) {

        this.calendario.agregarDia(fecha, horaIni, horaFin);

    }

    //ABSTRACT
    /*@Override
    public boolean DoLogin(String contraseña, String login) {
        boolean verific = false;

        if (this.getPass().equals(contraseña)) {
            verific = true;
        }

        return verific;
    }*/

}
