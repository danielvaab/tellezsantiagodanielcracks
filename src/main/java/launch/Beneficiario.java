package launch;

public class Beneficiario extends Paciente{
    private String documento;
    private String pass;
    private String login;
    private String numero="";
    private String genero="";
    private String fechanaa="";

    public Beneficiario() {
        super();
        this.documento = "";
        this.pass = "";
        this.login = "";
        this.numero = "";
        this.genero = "";
        this.fechanaa = "";
    }

    public Beneficiario(String pass, String login, String documento, String numero, String genero, String fechanaa) {
        super();
        this.documento = documento;
        this.pass = pass;
        this.login = login;
        this.numero = numero;
        this.genero = genero;
        this.fechanaa = fechanaa;
    }

    public void setFechanaa(String fechanaa) {
        this.fechanaa = fechanaa;
    }

    public String getFechanaa() {
        return fechanaa;
    }

    @Override
    public String getPass() {
        return pass;
    }

    @Override
    public void setPass(String pass) {
        this.pass = pass;
    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public void setdocumento(String documento) {
        this.documento = documento;
    }

    @Override
    public boolean DoLogin(String pass, String login) {
        return pass.equals(this.pass) && login.equals(this.login);
    }

    @Override
    public String gettipousuario() {
        return "paciente";
    }

    @Override
    public String getNumero() {
        return numero;
    }

    @Override
    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public String getGenero() {
        return genero;
    }

    @Override
    public void setGenero(String genero) {
        this.genero = genero;
    }
}
