package launch;

public class Paciente extends Stakeholder{

    private String documento;
    private String pass;
    private String login;
    private String numero;
    private String direccion;
    private String genero;

    public Paciente() {
        super();
        this.documento = "";
        this.pass = "";
        this.login = "";
        this.numero = "";
        this.direccion = "";
        this.genero = "";
    }

    public Paciente(String pass, String login, String documento, String numero, String genero, String direccion) {
        super();
        this.documento = documento;
        this.pass = pass;
        this.login = login;
        this.numero = numero;
        this.direccion = direccion;
        this.genero = genero;
    }

    @Override
    public String getPass() {
        return pass;
    }

    @Override
    public void setPass(String pass) {
        this.pass = pass;
    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String getdocumento() {
        return documento;
    }

    @Override
    public void setdocumento(String documento) {
        this.documento = documento;
    }

    @Override
    public boolean DoLogin(String pass, String login) {
        return pass.equals(this.pass) && login.equals(this.login);
    }

    @Override
    public String gettipousuario() {
        return "paciente";
    }

    @Override
    public String getNumero() {
        return numero;
    }

    @Override
    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public String getGenero() {
        return genero;
    }

    @Override
    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public String getDireccion() {
        return direccion;
    }

    @Override
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
