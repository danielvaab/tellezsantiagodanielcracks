package launch;

public class Administrador {

    private String pass="";
    private String login="";
    private String documento1;
    private String numero1="";
    private String direccion1="";
    private String genero1="";


    public Administrador(String pass, String login, String documento1, String numero1, String genero1, String direccion1) {
        super();
        this.pass = pass;
        this.login = login;
        this.numero1 = numero1;
        this.direccion1 = direccion1;
        this.genero1 = genero1;
        this.documento1 = documento1;
    }

    public Administrador(){

    }


    //GETTERS

    public String getdocumento1() {
        return documento1;
    }

    public String getcontra(){
        return pass;
    }
    public String getinicio(){
        return login;
    }
    public String gettiuser(){
        return "administrador";
    }
    public String getnumero() {
        return numero1;
    }
    public String getdireccion() {
        return direccion1;
    }

    public String getgenero() {
        return genero1;
    }

    //SETTERS
    public void setcontra(String pass){
        this.pass = pass;
    }

    public void setinicio(String login){
        this.login = login;
    }

    public void setdocument1(String document){
        this.documento1 = document;
    }

    public void setdireccion(String direccion1) {
        this.direccion1 = direccion1;
    }

    public void setGenero(String genero) {
        this.genero1 = genero1;
    }

    public void setnumero(String numero) {
        this.numero1 = numero1;
    }


    public boolean Doinicio(String pass, String login){
        /*retorna necesariamente true en caso de ser iguales y falso si no*/
        return pass.equals(this.pass) && login.equals(this.login);
    }
}
