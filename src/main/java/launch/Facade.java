package launch;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Facade {
    private Medico doctor;
    private Paciente enfermo;
    private ArrayList<Medico> medicos;
    private ArrayList<Paciente> pacientes;
    private ArrayList<Stakeholder> stakh;
    private ArrayList<Contrato> contratos;
    private boolean loggedAsMedic;
    private boolean loggedAsPatient;
    private Stakeholder medico1;
    private Cotizante cotizante;
    private Stakeholder admin1;
    ArrayList<Cotizante> coti;
    ArrayList<Administrador> administrador1;
    private static Facade facade = null;

    Medico medico2 = new Medico("1234", "33333", "Robert", "000000", "Macho", "Cll123");
    Medico medico3 = new Medico("6789", "55555", "Federico", "999999", "Macho", "Cll456");

    Administrador admin2 = new Administrador("1234","Andrés","9999","0000","Hombre","Calle Bogotá");
    Cotizante cotiz2 = new Cotizante("Cotizante", "9129", "Juan","8888","3131313","Masculino","Crra 9ba");
    Cotizante cotiz3 = new Cotizante("Cotizante", "1111", "Camila","0000","329423323","Femenino","Octava");
    //PRUEBAS PARA VER SI SERVLET SIRVE


    public static Facade getInstance() {
        if(facade == null) {
            facade = new Facade();
        }
        return facade;
    }
    private Facade() {

        pacientes = new ArrayList<>();
        medicos = new ArrayList<>();
        stakh = new ArrayList<>();
        medico1 = new Medico();
        cotizante = new Cotizante();
        admin1 = new AdapterAdministrador();
        coti = new ArrayList<>();
        administrador1 = new ArrayList<>();
        contratos = new ArrayList<>();

        this.doctor = null;
        this.enfermo = null;
        this.medicos = new ArrayList();
        this.pacientes = new ArrayList();
        this.loggedAsMedic = false;
        this.loggedAsPatient = false;



        medicos.add(medico2);
        medicos.add(medico3);

        administrador1.add(admin2);

        coti.add(cotiz2);
        coti.add(cotiz3);
    }

    public void nuevoMedico(String documento, String contraseña, String nombre, String numero, String direccion, String genero) {

        Medico doctor = new Medico(documento, contraseña, nombre, numero, direccion, genero);
        this.medicos.add(doctor);
        System.out.println("Wenas, Doctor");

    }

    public void nuevoPaciente(String documento, String contraseña, String nombre, String numero, String direccion, String genero) {

        Paciente pacien = new Paciente(documento, contraseña, nombre, numero, direccion, genero);
        this.pacientes.add(pacien);
        System.out.println("Nuevo paciente");

    }

    public void ingresar(int type, String nombre, String contraseña) {

        switch (type) {
            case 1:
                if (this.loggedAsPatient) {
                    System.out.println("Interesante");
                } else {
                    for (Medico med : this.medicos) {
                        if (med.getLogin().equals(nombre) && med.getPass().equals(contraseña)) {
                            this.doctor = med;
                            this.loggedAsMedic = true;
                            System.out.println("Ingresado");
                        }
                    }
                    break;
                }

            case 2:
                if (this.loggedAsMedic) {
                    System.out.println("Interesante");
                } else {
                    for (Paciente pac : this.pacientes) {
                        if (pac.getLogin().equals(nombre) && pac.getPass().equals(contraseña)) {
                            this.enfermo = pac;
                            this.loggedAsPatient = true;
                            System.out.println("Ingresado");
                        }
                    }
                    break;
                }

            default:
                System.out.println("Error!!!");
                break;
        }

    }

    public void verDisponibilidad(String medico, String fecha) {
        String disp = "";

        if (this.loggedAsPatient) {
            for (Medico med : this.medicos) {
                if (med.getLogin().equals(medico)) {
                    disp = med.getCalendario().verDisponibilidad(fecha);
                    System.out.println(disp);
                }
            }
        } else {
            System.out.println("Error!!!!");
        }
    }

    public void agregarDia(String fecha, float horaIni, float horaFin) {

        if (this.loggedAsMedic) {
            this.doctor.agregarDia(fecha, horaIni, horaFin);
            System.out.println("Se agregó el horario!");
        } else {
            System.out.println("Error!!!!");
        }
    }

    public void agendarCita(String fecha, float hora, String nombreM, String nombreP) {

        if (this.loggedAsPatient) {

            for (Medico med : this.medicos) {
                if (med.getLogin().equals(nombreM)) {
                    HashMap franjas = (HashMap) med.getCalendario().getDias().get(fecha);
                    Franja pedida = (Franja) franjas.get(hora);
                    if (pedida.getStatus().equals("Bloqueado") || pedida.getStatus().equals("Agendado")) {
                        System.out.println("Ocupada");
                    } else {
                        Cita citica = new Cita(med, this.enfermo);
                        pedida.setCitica(citica);
                        System.out.println("Cita agendada");
                    }
                }
            }
        } else {
            System.out.println("Error!!!No se puede agendar");
        }
    }

    public void logout() {
        if (this.loggedAsMedic) {
            this.doctor = null;
            this.loggedAsMedic = false;
        } else if (this.loggedAsPatient) {
            this.enfermo = null;
            this.loggedAsPatient = false;
        } else {
            System.out.println("Error!!!");
        }
    }

    //------------------GETTERS Y SERTTTERS
    public Medico getDoctor() {
        return doctor;
    }

    public Paciente getEnfermo() {
        return enfermo;
    }

    public ArrayList<Medico> getMedicos() {
        return medicos;
    }

    public ArrayList<Paciente> getPacientes() {
        return pacientes;
    }

    public boolean isLoggedAsMedic() {
        return loggedAsMedic;
    }

    public boolean isLoggedAsPatient() {
        return loggedAsPatient;
    }

    public ArrayList<Stakeholder> getStakh() {
        return stakh;
    }

    public Stakeholder getMedico1() {
        return medico1;
    }

    public Cotizante getCotizante() {
        return cotizante;
    }

    public Stakeholder getAdmin1() {
        return admin1;
    }

    public ArrayList<Cotizante> getCoti() {
        return coti;
    }

    public ArrayList<Administrador> getAdministrador1() {
        return administrador1;
    }

    //MÉTODOS OTROS SERVLETS

    public void VerStakeholders(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ServletOutputStream out = resp.getOutputStream();

        Gson gson = new Gson();
        Gson gson1 = new Gson();

        String id = req.getParameter("idstakeh");
        String stake = req.getParameter("selectstake");
        String accion = req.getParameter("selectaccionstake");

        if (stake.equals("PACIENTE")) {
            if (accion.equals("VER")) {
                try {
                    for (Cotizante cotiza : coti) {
                        System.out.println("HOLAzzz");
                        if (id.equals(cotiza.getdocumento())) {
                            System.out.println("xxxxx");
                            JsonElement nombrejson = gson.toJsonTree(cotiza);
                            JsonObject json = new JsonObject();
                            json.add("cotizante", nombrejson);//{medico:{nombre:juan,login:uan12,}}

                            out.write(json.toString().getBytes());
                            out.flush();
                            out.close();
                            System.out.println(cotiza.getLogin() + cotiza.getDireccion() + cotiza.getNumero() + cotiza.getGenero() + cotiza.getPass());
                        }
                    }
                } catch (Exception e) {
                    System.out.println("HOLA");
                    e.printStackTrace();
                }
            }
        }

        if (stake.equals("ADMINISTRADOR")) {
            if (accion.equals("VER")) {

                try {
                    for (Administrador adminis : administrador1) {
                        if (id.equals(adminis.getdocumento1())) {
                            JsonElement nombrejson = gson.toJsonTree(adminis);
                            JsonObject json = new JsonObject();
                            json.add("adapterAdministrador", nombrejson);//{medico:{nombre:juan,login:uan12,}}

                            out.write(json.toString().getBytes());
                            out.flush();
                            out.close();
                            System.out.println(adminis.getinicio() + adminis.getdireccion() + adminis.getnumero() + adminis.getgenero() + adminis.getcontra());
                        }
                    }
                } catch (Exception e) {
                    System.out.println("HOLA");
                    e.printStackTrace();
                }
            }
        }
        if (stake.equals("MEDICO")) {
            if (accion.equals("VER")) {

                try {
                    for (Medico med : medicos) {
                        if (id.equals(med.getdocumento())) {
                            JsonElement nombrejson = gson.toJsonTree(med);
                            JsonObject json = new JsonObject();
                            json.add("medico", nombrejson);//{medico:{nombre:juan,login:uan12,}}

                            out.write(json.toString().getBytes());
                            out.flush();
                            out.close();
                            System.out.println(med.getLogin() + med.getDireccion() + med.getNumero() + med.getGenero() + med.getPass());
                        }
                    }
                } catch (Exception e) {
                    System.out.println("HOLA");
                    e.printStackTrace();
                }
            }
            if (accion.equals("VER")) {

                try {
                    for (Medico med : medicos) {
                        if (id.equals(med.getdocumento())) {
                            JsonElement nombrejson = gson.toJsonTree(med);
                            JsonObject json = new JsonObject();
                            json.add("medico", nombrejson);//{medico:{nombre:juan,login:uan12,}}

                            out.write(json.toString().getBytes());
                            out.flush();
                            out.close();
                            System.out.println(med.getLogin() + med.getDireccion() + med.getNumero() + med.getGenero() + med.getPass());
                        }
                    }
                } catch (Exception e) {
                    System.out.println("HOLA");
                    e.printStackTrace();
                }
            }
        }
    }
//------------------------------------------------------------------------CREAR STAKEH
    public void CrearStakeH(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ServletOutputStream out = resp.getOutputStream();

        Gson gson = new Gson();
        Gson gson1 = new Gson();
        System.out.println("Parametros:");
        Map<String,String[]> params = req.getParameterMap();
        for(Map.Entry<String,String[]> e: params.entrySet()){
            System.out.println(e.getKey() + ":" + e.getValue()[0]);
        }
        System.out.println("-----------------------");
        String id = req.getParameter("idstakeh");

        //PRUBAS PARA VER SI SERVLET SIRVE
        //PRUEBAS PARA VER SI SERVLET SIRVE

        String stake = req.getParameter("selectstake");
        String accion = req.getParameter("selectaccionstake");

        if (stake.equals("PACIENTE")){
            if (accion.equals("AÑADIR")){
                try {
                    boolean valida=true;
                    for (Cotizante cotiza : coti) {
                        if (id.equals(cotiza.getdocumento())) {
                            valida=false;
                            System.out.println("HOLAAHPTa");
                        }
                    }
                    if(valida){
                        String id1 = req.getParameter("idstakeh");
                        String nombre = req.getParameter("nombrestakeh");
                        String direccion = req.getParameter("direccionstakeh");
                        String tel = req.getParameter("telefonostakeh");
                        String gen = req.getParameter("generostakeh");
                        String pass = req.getParameter("passstakeh");
                        Cotizante newcoti = new Cotizante("Cotizante", pass, nombre, id1, tel, gen, direccion);
                        coti.add(newcoti);
                        System.out.println("-----------------");
                        System.out.println(id1+nombre+direccion+tel+gen+pass);
                        System.out.println("_________________");
                    }
                    out.write("EXITOSO".getBytes());
                    out.flush();
                    out.close();
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (stake.equals("ADMINISTRADOR")) {
            if (accion.equals("AÑADIR")){
                try {
                    boolean valida=true;
                    for (Administrador administra : administrador1) {
                        if (id.equals(administra.getdocumento1())) {
                            valida=false;
                            System.out.println("HOLAAHPTa");
                        }
                    }
                    if(valida){
                        String id1 = req.getParameter("idstakeh");
                        String nombre = req.getParameter("nombrestakeh");
                        String direccion = req.getParameter("direccionstakeh");
                        String tel = req.getParameter("telefonostakeh");
                        String gen = req.getParameter("generostakeh");
                        String pass = req.getParameter("passstakeh");
                        Administrador newadmin = new Administrador(pass, nombre, id1, tel, gen, direccion);
                        administrador1.add(newadmin);
                        System.out.println("-----------------");
                        System.out.println(id1+nombre+direccion+tel+gen+pass);
                        System.out.println("_________________");
                    }
                    out.write("EXITOSO".getBytes());
                    out.flush();
                    out.close();
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (stake.equals("MEDICO")) {
            if (accion.equals("AÑADIR")){
                try {
                    boolean valida=true;
                    for (Medico med : medicos) {
                        if (id.equals(med.getdocumento())) {
                            valida=false;
                            System.out.println("HOLAAHPTa");
                        }
                    }
                    if(valida){
                        String id1 = req.getParameter("idstakeh");
                        String nombre = req.getParameter("nombrestakeh");
                        String direccion = req.getParameter("direccionstakeh");
                        String tel = req.getParameter("telefonostakeh");
                        String gen = req.getParameter("generostakeh");
                        String pass = req.getParameter("passstakeh");
                        Medico newmed = new Medico(pass, nombre, id1, tel, gen, direccion);
                        medicos.add(newmed);
                        System.out.println("-----------------");
                        System.out.println(id1+nombre+direccion+tel+gen+pass);
                        System.out.println("_________________");
                    }
                    out.write("EXITOSO".getBytes());
                    out.flush();
                    out.close();
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void GestionContrato(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ServletOutputStream out = resp.getOutputStream();

        Gson gson = new Gson();
        Gson gson1 = new Gson();

        String codContr = req.getParameter("codcont");

        try {
            boolean valida=true;
            for (Contrato contra : contratos) {
                if (codContr.equals(contra.getCodCont())) {
                    valida=false;
                    System.out.println("HOLAAHPTa");
                }
            }
            if(valida){
                String codCont = req.getParameter("codcont");

                String idBene = req.getParameter("idcont");
                String nombreBene = req.getParameter("nombrecont");
                String FechanNac = req.getParameter("fechanaccont");
                String telBene = req.getParameter("telefonocont");
                String genBene = req.getParameter("generocont");
                String contBene = req.getParameter("contracont");

                String ips1 = req.getParameter("codips");
                String eps1 = req.getParameter("epscont");

                Beneficiario ben = new Beneficiario(idBene,contBene,nombreBene,telBene,genBene,FechanNac);
                Cotizante newcoti = new Cotizante("Cotizante", "12345", "Federico V", "1344114", "34184813813", "Masculino", "Calle 913e x");

                Component cont = new Contrato(codCont);

                EPS newips = new EPS(ips1);

                EPS neweps = new EPS(eps1);

                ((Contrato) cont).adicionarben(codCont, ben);
                ((Contrato) cont).setCot(newcoti);

                neweps.adicionarcomp(newips);
                neweps.adicionarcomp(cont);
                newips.adicionarcomp(cont);

                System.out.println(neweps.mostrarcon());
                System.out.println(neweps.mostrarper(codCont));
            }
            out.write("EXITOSO".getBytes());
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
