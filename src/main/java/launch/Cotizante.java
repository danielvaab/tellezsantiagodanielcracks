package launch;

public class Cotizante extends Paciente{
    private String kindauser;

    public Cotizante() {
        super();
        this.kindauser = "";
    }

    public Cotizante(String kindauser, String pass, String login, String documento, String numero, String genero, String direccion) {
        super(pass, login, documento, numero, genero, direccion);
        this.kindauser = kindauser;
    }

    public void setKindauser(String kindauser) {
        this.kindauser = kindauser;
    }

    public String getKindauser() {
        return kindauser;
    }
}
