package launch;

import java.util.HashMap;

public class Calendario {

    private HashMap dias;

    public Calendario() {
        this.dias = dias;
    }

    public void agregarDia (String fecha, float horaIni, float horaFin){
        HashMap franjas = new HashMap();

        for (float i = (float)7.0; i <= 18.5; i += 0.5){
            Franja franjita;
            if(i < horaIni || i > horaFin){
                franjas.put(i, franjita = new Franja("Bloqueado"));
            }
            else{
                franjas.put(i, franjita = new Franja("Disponible"));
            }
        }
        this.dias.put(fecha,franjas);
    }

    public String verDisponibilidad(String fecha){
        String disp ="";

        disp = this.dias.get(fecha).toString();
        return disp;
    }

    public HashMap getDias() {
        return dias;
    }

    public void setDias(HashMap dias) {
        this.dias = dias;
    }
}
