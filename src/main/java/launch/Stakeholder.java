package launch;

public abstract class Stakeholder {

    public abstract String getPass();
    public abstract void setPass(String pass);
    public abstract String getLogin();
    public abstract void setLogin(String login);
    public abstract String getdocumento();
    public abstract void setdocumento(String documento);
    public abstract boolean DoLogin(String pass, String login);
    public abstract String gettipousuario();
    public abstract String getNumero();
    public abstract void setNumero(String numero);
    public abstract String getGenero();
    public abstract void setGenero(String genero);
    public abstract String getDireccion();
    public abstract void setDireccion(String direccion);


}
