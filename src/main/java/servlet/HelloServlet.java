package servlet;

import launch.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@WebServlet(
        name = "MyServlet", 
        urlPatterns = {"/ejemplo"}
    )
public class HelloServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ServletOutputStream out = resp.getOutputStream();
        Gson gson = new Gson();

        String stake = req.getParameter("selectstake");
        String accion = req.getParameter("selectaccionstake");

        String id=req.getParameter("idstakeh");
        String nombre=req.getParameter("nombrestakeh");
        String direccion=req.getParameter("direccionstakeh");
        String tel=req.getParameter("telefonostakeh");
        String gen=req.getParameter("generostakeh");
        String pass=req.getParameter("passstakeh");



        //PRUBAS PARA VER SI SERVLET SIRVE
        Medico medico2 = new Medico("1234", "33333", "Robert",  "000000", "Macho", "Cll123");
        Medico medico3 = new Medico("6789", "55555", "Federico",  "999999", "Macho", "Cll456");

        //PRUEBAS PARA VER SI SERVLET SIRVE

        Stakeholder medico1 = new Medico();
        Paciente cotizante = new Cotizante();
        Stakeholder admin1 = new AdapterAdministrador();
        ArrayList<Stakeholder> stakh = new ArrayList<>();
        ArrayList<Medico> medico = new ArrayList<>();
        ArrayList<Paciente> paciente = new ArrayList<>();

        medico.add(medico2);
        medico.add(medico3);
        ArrayList<AdapterAdministrador> admin = new ArrayList<>();





        if (stake.equals("PACIENTE")){
            if (accion=="AÑADIR") {

                cotizante.setLogin(nombre);
                cotizante.setDireccion(direccion);
                cotizante.setGenero(gen);
                cotizante.setdocumento("Cotizante");
                cotizante.setNumero(tel);
                cotizante.setPass(pass);
            }
            if (accion.equals("VER")){
                try {
                    for (Paciente pac : paciente) {
                        if (!id.equals(pac.getdocumento())) {
                            req.setAttribute("nombrestakeh", pac.getLogin());
                            req.setAttribute("direccionstakeh", pac.getDireccion());
                            req.setAttribute("telefonostakeh", pac.getNumero());
                            req.setAttribute("generostakeh", pac.getGenero());
                            req.setAttribute("passstakeh", pac.getPass());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            }
            if (accion.equals("CAMBIAR")){

            }// IMPORTANTE!!!!!!!!! HAY QUE EJECUTAR OTRO SERVLET PARA QUE OBTENGA LOS DATOS DEL FORMULARIO Y LOS GUARDE!
            if (accion=="ELIMINAR"){

            }


        if (stake.equals("MEDICO")) {
            if (accion.equals("AÑADIR")) {
                try {
                    for (Medico med : medico) {
                        if (!id.equals(med.getdocumento())) {
                            medico1.setLogin(nombre);
                            medico1.setDireccion(direccion);
                            medico1.setGenero(gen);
                            medico1.setdocumento(id);
                            medico1.setNumero(tel);
                            medico1.setPass(pass);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace(); // NO SÉ SI SEA ASIIIIIIIIIIIIIIIIIIIIIIIIÍ
                }
            }
            if (accion.equals("VER")) {
                try {
                    for (Medico med : medico) {
                        if (!id.equals(med.getdocumento())) {
                            req.setAttribute("nombrestakeh", med.getLogin());
                            req.setAttribute("direccionstakeh", med.getDireccion());
                            req.setAttribute("telefonostakeh", med.getNumero());
                            req.setAttribute("generostakeh", med.getGenero());
                            req.setAttribute("passstakeh", med.getPass());

                            JsonElement nombrejson = gson.toJsonTree(medico1);
                            JsonObject json = new JsonObject();
                            json.add("medico", nombrejson);//{medico:{nombre:juan,login:uan12,}}
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
                if (accion.equals("CAMBIAR")) {
                    try {
                        for (Medico med : medico) {
                            if (!id.equals(med.getdocumento())) {
                                req.setAttribute("nombrestakeh", med.getLogin());
                                req.setAttribute("direccionstakeh", med.getDireccion());
                                req.setAttribute("telefonostakeh", med.getNumero());
                                req.setAttribute("generostakeh", med.getGenero());
                                req.setAttribute("passstakeh", med.getPass());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    // IMPORTANTE!!!!!!!!! HAY QUE EJECUTAR OTRO SERVLET PARA QUE OBTENGA LOS DATOS DEL FORMULARIO Y LOS GUARDE!
                }
                if (accion.equals("ELIMINAR")) {

                }
            }
        if (stake.equals("ADMINISTRADOR")){
            if (accion=="AÑADIR") {
                for (AdapterAdministrador administra :admin) {
                    if(!id.equals(administra.getdocumento())) {
                        admin1.setLogin(nombre);
                        admin1.setDireccion(direccion);
                        admin1.setGenero(gen);
                        admin1.setdocumento(id);
                        admin1.setNumero(tel);
                        admin1.setPass(pass);
                        }
                    }
            }
            if (accion.equals("VER")){
                for (AdapterAdministrador administra :admin) {
                    if(id.equals(administra.getdocumento())) {

                    }
                }
            }
            if (accion.equals("CAMBIAR")){

            } // IMPORTANTE!!!!!!!!! HAY QUE EJECUTAR OTRO SERVLET PARA QUE OBTENGA LOS DATOS DEL FORMULARIO Y LOS GUARDE!
            if (accion=="ELIMINAR"){

            }
        }
    }
    
}
