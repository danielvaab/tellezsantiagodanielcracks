package servlet;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import launch.*;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

@WebServlet(
        name = "MyServlet1",
        urlPatterns = {"/StakehServlet"}
    )
public class StakehServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Facade facade = Facade.getInstance();
        facade.VerStakeholders(req, resp);
        ServletOutputStream out = resp.getOutputStream();
        Gson gson = new Gson();
        Gson gson1 = new Gson();
        System.out.println("Parametros:");
        Map<String,String[]> params = req.getParameterMap();
        for(Map.Entry<String,String[]> e: params.entrySet()){
            System.out.println(e.getKey() + ":" + e.getValue()[0]);
        }
        System.out.println("-----------------------");
        String id = req.getParameter("idstakeh");
        /*String nombre = req.getParameter("nombrestakeh");
        String direccion = req.getParameter("direccionstakeh");
        String tel = req.getParameter("telefonostakeh");
        String gen = req.getParameter("generostakeh");
        String pass = req.getParameter("passstakeh");
*/
/*
        //PRUBAS PARA VER SI SERVLET SIRVE
        Medico medico2 = new Medico("1234", "33333", "Robert", "000000", "Macho", "Cll123");
        Medico medico3 = new Medico("6789", "55555", "Federico", "999999", "Macho", "Cll456");

        Administrador admin2 = new Administrador("1234","Andrés","9999","0000","Hombre","Calle Bogotá");
        Cotizante cotiz2 = new Cotizante("Cotizante", "9129", "Juan","8888","3131313","Masculino","Crra 9ba");
        Cotizante cotiz3 = new Cotizante("Cotizante", "1111", "Camila","0000","329423323","Femenino","Octava");
        //PRUEBAS PARA VER SI SERVLET SIRVE



        medico.add(medico2);
        medico.add(medico3);

        administrador1.add(admin2);

        coti.add(cotiz2);
        coti.add(cotiz3);*/

        ArrayList<AdapterAdministrador> admin = new ArrayList<>();

        String stake = req.getParameter("selectstake");
        String accion = req.getParameter("selectaccionstake");

        /*if (stake.equals("PACIENTE")){
            if (accion.equals("VER")) {
                try {
                    for (Cotizante cotiza : coti) {
                        System.out.println("HOLAzzz");
                        if (id.equals(cotiza.getdocumento())) {
                            System.out.println("xxxxx");
                            JsonElement nombrejson = gson.toJsonTree(cotiza);
                            JsonObject json = new JsonObject();
                            json.add("cotizante", nombrejson);//{medico:{nombre:juan,login:uan12,}}

                            out.write(json.toString().getBytes());
                            out.flush();
                            out.close();
                            System.out.println(cotiza.getLogin() + cotiza.getDireccion() + cotiza.getNumero() + cotiza.getGenero() + cotiza.getPass());
                        }
                    }
                } catch (Exception e) {
                    System.out.println("HOLA");
                    e.printStackTrace();
                }
        }}

        if (stake.equals("ADMINISTRADOR")) {
            if (accion.equals("VER")) {

                try {
                    for (Administrador adminis : administrador1) {
                        if (id.equals(adminis.getdocumento1())) {
                            JsonElement nombrejson = gson.toJsonTree(adminis);
                            JsonObject json = new JsonObject();
                            json.add("adapterAdministrador", nombrejson);//{medico:{nombre:juan,login:uan12,}}

                            out.write(json.toString().getBytes());
                            out.flush();
                            out.close();
                            System.out.println(adminis.getinicio() + adminis.getdireccion() + adminis.getnumero() + adminis.getgenero() + adminis.getcontra());
                        }
                    }
                } catch (Exception e) {
                    System.out.println("HOLA");
                    e.printStackTrace();
                }
            }
        }
        if (stake.equals("MEDICO")) {

            /*

            if (accion.equals("AÑADIR")) {
                try {
                    for (Medico med : medico) {
                        if (!id.equals(med.getdocumento())) {
                            medico1.setLogin(nombre);
                            medico1.setDireccion(direccion);
                            medico1.setGenero(gen);
                            medico1.setdocumento(id);
                            medico1.setNumero(tel);
                            medico1.setPass(pass);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace(); // NO SÉ SI SEA ASIIIIIIIIIIIIIIIIIIIIIIIIÍ
                }
            }
            if (accion.equals("VER")) {

                try {
                    for (Medico med : medico) {
                        if (id.equals(med.getdocumento())) {
                            JsonElement nombrejson = gson.toJsonTree(med);
                            JsonObject json = new JsonObject();
                            json.add("medico", nombrejson);//{medico:{nombre:juan,login:uan12,}}

                            out.write(json.toString().getBytes());
                            out.flush();
                            out.close();
                            System.out.println(med.getLogin() + med.getDireccion() + med.getNumero() + med.getGenero() + med.getPass());
                        }
                    }
                } catch (Exception e) {
                    System.out.println("HOLA");
                    e.printStackTrace();
                }
            }*//*
            if (accion.equals("CAMBIAR")) {
                try {
                    for (Medico med : medico) {
                        if (!id.equals(med.getdocumento())) {
                            req.setAttribute("nombrestakeh", med.getLogin());
                            req.setAttribute("direccionstakeh", med.getDireccion());
                            req.setAttribute("telefonostakeh", med.getNumero());
                            req.setAttribute("generostakeh", med.getGenero());
                            req.setAttribute("passstakeh", med.getPass());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // IMPORTANTE!!!!!!!!! HAY QUE EJECUTAR OTRO SERVLET PARA QUE OBTENGA LOS DATOS DEL FORMULARIO Y LOS GUARDE!
            }*//*
            if (accion.equals("ELIMINAR")) {

            }*/
        }

    }
