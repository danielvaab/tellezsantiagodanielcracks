package servlet;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import launch.*;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

@WebServlet(
        name = "MyServlet3",
        urlPatterns = {"/ServletContrato"}
    )
public class ContratoServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Facade facade = Facade.getInstance();
        facade.GestionContrato(req, resp);
        ServletOutputStream out = resp.getOutputStream();
        Gson gson = new Gson();
        Gson gson1 = new Gson();
        System.out.println("Parametros:");
        Map<String,String[]> params = req.getParameterMap();
        for(Map.Entry<String,String[]> e: params.entrySet()){
            System.out.println(e.getKey() + ":" + e.getValue()[0]);
        }
        System.out.println("-----------------------");
    }
    
}
